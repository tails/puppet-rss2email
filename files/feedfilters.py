"""
rss2email custom post-process filters.
"""

import re


def drop_non_security(message, **kwargs):
    """
    Return the message only if it contains a case-insensitive 'security' word
    in either its subject or payload. Else, return None.
    """
    payload = message.get_payload()
    subject = str(message['subject'])
    haystack = payload + subject
    needle = 'security'
    if re.search(needle, haystack, re.IGNORECASE):
        return message
    return None
