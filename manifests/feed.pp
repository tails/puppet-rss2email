# @summary
#   Subscribe to an rss feed.
#
# @example
#   rss2email::feed { 'foo':
#     url     => 'https://tails.boum.org/news/index.en.rss',
#     from    => 'sender@example.org',
#     to      => 'recipient@example.org',
#     filters => {
#       drop_non_security => present,
#     },
#   }
#
# @param url
#   The url of the feed to be subscribed to.
#
# @param from
#   The email address used in the From header.
#
# @param to
#   The email address mails are sent to.
#
# @param filters
#   A hash with the filters that should be present or absent for this feed.
#
define rss2email::feed (
  String               $url,
  String               $from    = $rss2email::default_from,
  String               $to      = $rss2email::default_to,
  Hash[String, String] $filters = {},
) {
  $user = $rss2email::user
  $database = $rss2email::database
  $config_file = $rss2email::config_file

  # add feed unless already present in database
  exec { "r2e add ${name}":
    command => "/usr/bin/r2e add '${name}' '${url}' '${to}'",
    unless  => "/usr/bin/jq < ${database} '.feeds[] | select(.name == \"${name}\")'",
    require => [Package['jq'], Exec['r2e new']],
    user    => $user,
  }

  $settings = {
    'url'         => $url,
    'from'        => $from,
    'to'          => $to,
    'name-format' => '{feed-title}',
  }

  # ensure up-to-date feed config
  create_ini_settings( { "feed.${name}" => $settings },
    { path => $config_file, require => Exec["r2e add ${name}"] }
  )

  $filters.each | String $filtername, String $ensure | {
    rss2email::filter { "${filtername} for ${name}":
      ensure    => $ensure,
      feed_name => $name,
      filter    => $filtername,
      require   => Exec["r2e add ${name}"],
    }
  }
}
