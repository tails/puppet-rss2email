# @summary
#   Receive RSS feeds by e-mail
#
# @example
#   class { 'rss2email':
#     default_from => 'sender@example.org',
#     default_to   => 'recipient@example.org',
#   }
#
# @param default_from
#   The email address used by default in the From header.
#
# @param default_to
#   The email address mails are sent to by default.
#
# @param user
#   The POSIX user running r2e
#
# @param homedir
#   The homedir for abovementioned user.
#
# @param config_file
#   The location of the config file.
#
# @param database
#   The location of the database file.
#
# This class ensures the installation and base configuration of rss2mail.
# Subscriptions to feeds can be managed through the rss2email::feed resource.
#
class rss2email (
  String $default_from,
  String $default_to,
  String $user                      = 'rss2email',
  Stdlib::Absolutepath $homedir     = "/var/lib/${user}",
  Stdlib::Absolutepath $config_file = "${homedir}/.config/rss2email.cfg",
  Stdlib::Absolutepath $database    = "${homedir}/.local/share/rss2email.json",
) {
  ensure_packages(['rss2email', 'jq'])

  user { $user:
    ensure     => present,
    home       => $homedir,
    managehome => true,
    system     => true,
  }

  # initialize unless config file is already present
  exec { 'r2e new':
    command => "/usr/bin/r2e new ${default_to}",
    creates => "${homedir}/.config/rss2email.cfg",
    user    => $user,
    require => [Package['rss2email'], User[$user]],
  }

  systemd::timer { 'rss2email.timer':
    timer_source    => 'puppet:///modules/rss2email/rss2email.timer',
    service_content => epp('rss2email/rss2email.service.epp', { user => $user }),
    active          => true,
    enable          => true,
  }

  #
  # Feed filters
  #

  $python_path = '/usr/local/lib/python3/dist-packages'

  file { [
      '/usr/local/lib',
      '/usr/local/lib/python3',
      $python_path,
    ]:
      ensure => directory,
      owner  => root,
      group  => staff,
      mode   => '2755',
  }

  file { "${python_path}/feedfilters.py":
    source  => 'puppet:///modules/rss2email/feedfilters.py',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$python_path],
  }
}
