# @summary
#   Install custom rss2email post-process filters
#
# @api private
#
# @param feed_name
#   The name of the feed to be filtered.
#
# @param filter
#   The name of the filter to be applied to the feed.
#
# @param ensure
#   Whether the filter should be present or absent.
#
define rss2email::filter (
  String $feed_name,
  String $filter,
  String $ensure = present,
) {
  ini_setting { "rss2email feed filter: ${name}":
    ensure  => $ensure,
    path    => $rss2email::config_file,
    section => "feed.${feed_name}",
    setting => 'post-process',
    value   => "feedfilters ${filter}",
  }
}
