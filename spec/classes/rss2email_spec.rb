# frozen_string_literal: true

require 'spec_helper'

describe 'rss2email' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          'default_from' => 'sender@example.org',
          'default_to'   => 'recipient@example.org'
        }
      end

      it { is_expected.to compile }
    end
  end
end
