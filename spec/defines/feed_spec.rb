# frozen_string_literal: true

require 'spec_helper'

describe 'rss2email::feed' do
  let(:title) { 'myfeed' }
  let(:params) do
    {
      'url'     => 'https://tails.boum.org/news/index.en.rss',
      'filters' => {
        'drop_non_security' => 'present',
      }
    }
  end

  let(:pre_condition) do
    [
      "class { 'rss2email':
         default_from => 'sender@example.org',
         default_to   => 'recipient@example.org'
       }",
    ]
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
