# rss2email

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with rss2email](#setup)
    * [Beginning with rss2email](#beginning-with-rss2email)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)

## Description

This module installs the rss2email package and manages subscriptions to feeds.

## Setup

### Beginning with rss2email

To install rss2email and ensure a base configuration, you must provide default
to and from addresses in hiera:

```
rss2email::default_from: sender@example.org
rss2email::default_to: recipient@example.org
```

Then, simply include the class:

```
include rss2email
```

## Usage

To subscribing to a feed, call the feed resource:

```
rss2email::feed { 'foo':
  url     => 'https://tails.boum.org/news/index.en.rss',
  filters => {
    drop_non_security => present,
  },
}
```

## Limitations

  - Currently only filtering for security related content is supported
