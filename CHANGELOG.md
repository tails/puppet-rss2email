# Changelog

## Release 0.1.0

Hooray, the first release!

**Features**

  - Installing and configuring rss2email
  - Subscribing to feeds
  - Basic filtering of feeds

**Known Issues**

  - Currently only filtering for security related content is supported
